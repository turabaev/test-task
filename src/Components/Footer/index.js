import React from 'react';
import styles from './Footer.module.scss'

const Footer = () => {
    return (
        <footer>
            <div className={styles.footerWrapper + ' content'}>
                <div className={styles.footerLeft}>
                    <p>© 2012–2016 ЗАО «Компания»</p>
                    <span>info@name.ru</span>
                </div>
                <div className={styles.footerMiddle}>
                    <ul>
                        <li>
                            <a href="#">
                            Главная
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Каталог
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Доставка и оплата
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Прайс-лист
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Контакты
                            </a>
                        </li>
                    </ul>
                </div>
                <div className={styles.footerRight}>
                    <a href="#">
                        Разработка сайта -
                    </a>
                    <p>компания "Шнайдер Консалт"</p>
                </div>
            </div>
        </footer>
    );
};

export default Footer;