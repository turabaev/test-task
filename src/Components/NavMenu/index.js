import React from 'react';
import styles from './NavMenu.module.scss'

const NavMenu = () => {
    return (
        <nav>
            <div className="content">
                <ul>
                    <li>
                        <a href="#">
                        Главная
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        Каталог
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        Доставка и оплата
                    </a>
                    </li>
                    <li>
                        <a href="#">
                        Прайс-лист
                        </a>
                    </li>
                    <li>
                        <a href="#">
                        Контакты
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    );
};

export default NavMenu;