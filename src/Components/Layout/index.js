import React from 'react';
import Header from "../Header";
import NavMenu from "../NavMenu";
import Slider from "../Slider";
import Footer from "../Footer";

const Layout = ({children}) => {
    return (
        <>
            <Header/>
            <NavMenu/>
            <Slider/>
            <main>
                {children}
            </main>
            <Footer/>
        </>
    );
};

export default Layout;