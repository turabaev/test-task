import React from 'react';
import styles from './Header.module.scss'
import Logo from '../../assets/logo.png'

const Header = () => {
    const [visibleData, setVisibleData] = React.useState()
    const date = new Date();

    React.useEffect(() => {
        let dateFormat = '.' + date.getMonth() + '.' + date.getFullYear()
        if(date.getDay()){
            let changeDay = (date.getDate() + 8 - date.getDay()) + dateFormat
            setVisibleData(changeDay)
        } else {
            setVisibleData((date.getDate() + 1) + dateFormat)
        }
    }, [])

    return (
        <header>
            <div className={styles.wrapperHeader + " content"}>
                <div className={styles.wrapperLeft}>
                    <img src={Logo} alt="Логотип" width="100%" height="100%" />

                </div>
                <div className={styles.wrapperMiddle}>
                    <p>Распродажа до {visibleData}!</p>
                </div>
                <div className={styles.wrapperRight}>
                    <a href="tel:+73432525252">+7 (343) 252 52 52</a>
                    <a href="tel:+73432525252">+7 (343) 252 52 52</a>
                </div>
            </div>
        </header>
    );
};

export default Header;