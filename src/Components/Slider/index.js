import React from 'react';
import Background from '../../assets/slider.png'

const Slider = () => {
    return (
        <div>
            <img src={Background} alt="Slider" width={'100%'} height={'450px'} />
        </div>
    );
};

export default Slider;